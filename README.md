# Wordpress deploy module

Deploy module is based on Ansible and it is compatibile with [wp-docker-starter](https://gitlab.com/GAAD/wp-docker-starter) based projects

## Usage
### Repository variables
 Deploy module works out of the box and its triggered by Gitlab CICD. Configuration is kept in repository variables.
 The list of variables that must be present in a repository:
 

Variable name | Description
------------ | -------------
ANSIBLE_HOST | remote server host
ANSIBLE_HOST_PORT | remote server host port
ANSIBLE_PYTHON_INTERPRETER | python interpreter path
ANSIBLE_SSH_PASS | remote server ssh password
ANSIBLE_SSH_USER | remote server ssh user
DB_HOST | Database host 
DB_NAME | Database name
DB_PASS | Database password
DB_USER | Database user
DEPLOYMENT_ROOT | absolute deployment root on remote server with trailing slash
DEPLOY_FROM_BRANCH | branch to deploy from
GITLAB_USERNAME | Gitlab username that is allowed to pull project
GITLAB_PASSWORD | Gitlab user password
GITLAB_REPO | Gitlab repository address
PHP_INTERPRETER | php interpreter on remote server eg.: php7
WORDPRESS_ADMIN_USER | Wordpress admin administrator user name
WORDPRESS_ADMIN_PASSWORD | Wordpress admin administrator user password
WORDPRESS_ADMIN_EMAIL | Wordpress admin administrator user email
WORDPRESS_SITEURL | Wordpress deployed site url
WORDPRESS_TABLES_PREFIX | Wordpress database tables prefix
WORDPRESS_VERSION | Wordpress version
WORDPRESS_LOCALE | Wordpress locale
BASIC_AUTH | .htpasswd file contents 

### Repository environments
* integration
* production

All variables need to be set either globally or be attached to those two environments  
* copy `.gitlab-ci.yml-dist` as `.gitlab-ci.yml` to your site root
    (when using wp-docker-starter it will be public_html directory)
* configure repository variables
* pushes to `develop` branch will trigger deployment to integration environment
* pushes to `master` branch will trigger deployment to production environment
